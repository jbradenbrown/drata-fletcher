use bytes;
use bytes::*;
use chrono::Utc;
use futures::executor::block_on;
use futures::io;
use futures::prelude::*;
use futures::stream;
use hyper::{client::HttpConnector, Body, Client};
use hyper_tls::HttpsConnector;
use reqwest;
use serde_json;
use serde_json::Value;
use sled;
use std::iter::Extend;
use std::iter::Iterator;
use std::time;
use tokio;

type HttpsClient = Client<hyper_tls::HttpsConnector<HttpConnector>>;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let https = HttpsConnector::new();
    let client = Client::builder().build::<_, hyper::Body>(https);

    let db = sled::open("blaseball")?;
    let mut interval = tokio::time::interval(time::Duration::from_secs(4));

    let stream = db.open_tree("stream")?;
    let players = db.open_tree("players")?;

    let mut counter: i64 = 0;
    loop {
        let timestamp = Utc::now().timestamp().to_string();
        let stream_data = get_stream(&client).await?;
        println!("Getting stream data... ({})", counter);

        if counter % 15 == 0 {
            let parsed_stream = serde_json::from_str::<serde_json::Value>(&stream_data)?;
            let player_data = get_players(&client, &parsed_stream).await?;
            // println!("{}\n\n", player_data);
            players.insert(&timestamp, player_data.as_bytes())?;
        }
        // println!("{}\n\n", stream_data);
        stream.insert(timestamp, stream_data.as_bytes())?;
        let flush = db.flush_async();
        counter += 1;
        interval.tick().await;

        if counter % 45 == 0 {
            flush.await?;
        }
    }

    Ok(())
}

async fn get_stream(client: &HttpsClient) -> Result<String, Box<dyn std::error::Error>> {
    let mut res = client
        .get("https://www.blaseball.com/events/streamData".parse()?)
        .await?;

    let body = res.body_mut();

    Ok(body
        .map(|item| stream::iter(item.unwrap()))
        .flatten()
        .skip_while(|x| future::ready(*x != '{' as u8))
        .take_while(|x| future::ready(*x != '\n' as u8))
        .map(|c| c as char)
        .collect::<String>()
        .await)
}

async fn get_players(
    client: &HttpsClient,
    stream_data: &Value,
) -> Result<String, Box<dyn std::error::Error>> {
    let teams = &stream_data["value"]["leagues"]["teams"].as_array().unwrap();
    let players = teams
        .iter()
        .map(|e| {
            [&e["lineup"], &e["rotation"], &e["bullpen"], &e["bench"]]
                .iter()
                .map(|e| e.as_array().unwrap())
                .flatten()
                .collect::<Vec<&Value>>()
        })
        .flatten()
        .map(|e| e.as_str().unwrap())
        .collect::<Vec<&str>>();

    let mut players_data: Vec<String> = Vec::new();
    for chunk in players.chunks(20) {
        let mut res = client
            .get(
                format!(
                    "https://www.blaseball.com/database/players?ids={}",
                    chunk.join(",")
                )
                .parse()?,
            )
            .await?;

        let players_part = res
            .body_mut()
            .map(|item| stream::iter(item.unwrap()))
            .flatten()
            .take_while(|x| future::ready(*x != '\n' as u8))
            .map(|c| c as char)
            .collect::<String>()
            .await;

        let players_part = players_part.trim_start_matches('[').trim_end_matches(']');

        players_data.push(players_part.to_string());
    }

    Ok(format!("[{}]", players_data.join(",")))
}

// 8839a3c4-721e-4c3c-9a5f-0ab6bd1115eb
// 8839a3c4-721e-4c3c-9a5f-0ab6bd1115eb
